package application;

import javafx.geometry.Bounds;
import javafx.scene.image.Image;

public class Coin extends GameCharacter {
	private static final String IMAGE_FILENAME = "images/coinGold.png";
	private static final double IMAGE_SIZE = 80;

	public Coin(double x, double y, Game game) {
		super(x, y, IMAGE_SIZE, game);
		System.out.println("COIN");
		setX(x);
		setY(y);
		imageSize = IMAGE_SIZE;
        imageHeight = IMAGE_SIZE;
        setFitWidth(imageSize);
        setFitHeight(imageHeight);
        Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_FILENAME));
        setImage(image);
	}
	
	@Override
	public void update(Level level, double elapsedTime) {
		Player player = game.mPlayer;
		Bounds bounds = player.getLayoutBounds();
		if (intersects(bounds)) {
			isAlive = false;
		}
		updateAliveStatus(level);
		
	}

	
	
}
