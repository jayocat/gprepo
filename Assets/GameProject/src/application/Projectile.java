package application;

import javafx.geometry.Bounds;
import javafx.scene.image.Image;

public class Projectile extends GameCharacter {
	private static final String IMAGE_FILENAME = "images/laserRedDot.png";
	private static final double IMAGE_SIZE = 8;
	
	private GameCharacter owner;
	
	public Projectile(double x, double y, Game game, GameCharacter owner) {
		super(x, y, IMAGE_SIZE, game);
		this.owner = owner;
		imageSize = IMAGE_SIZE;
        imageHeight = IMAGE_SIZE;
        setFitWidth(imageSize);
        setFitHeight(imageHeight);
        Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_FILENAME));
        setImage(image);
		xSpeed = DEFAULT_SPEED * 2;
		forward = owner.forward;
		isMovingRight = forward;
		isMovingLeft = !forward;
	}
	
	@Override
	public void update(Level level, double elapsedTime) {
		if (isMovingRight) {
            setX(getX() + xSpeed * elapsedTime); //framerate indpp.
        }
        if (isMovingLeft) {
            setX(getX() - xSpeed * elapsedTime);
        }
        for (Platform platform : level.getPlatformList()) {
            Bounds bounds = platform.getLayoutBounds();
            if (intersects(bounds) && platform.isBackground == false) {
               isAlive = false;
            }
        }
        if (getX() < 0) {
          isAlive = false; 
        }
        updateAliveStatus(level);
	}
}
