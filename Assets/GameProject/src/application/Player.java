package application;

import javafx.geometry.Bounds;
import javafx.scene.image.Image;

import java.util.Date;

class Player extends GameCharacter {
	private static String IMAGE_IDLE = "images/alienPink_stand";
	private static String IMAGE_WALKING = "images/alienPink_walk";
	private static String IMAGE_JUMPING = "images/alienPink_jump";
	private static String IMAGE_FALLING = "images/alienPink_falling";
	private static final double IMAGE_SIZE = 50;

	private static int SHOOTER_X_OFFSET = 37;
	private static int SHOOTER_Y_OFFSET = 42;

	private boolean isInvincible;

	private int lives;
	private int health;

	private Shooter shooter;

	Player(double x, double y, Game game) {
		super(x, y, IMAGE_SIZE, game);
		imageSize = IMAGE_SIZE;
		imageHeight = IMAGE_SIZE + 20;
		setFitWidth(imageSize);
		setFitHeight(imageHeight);
		Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_IDLE + ".png"));
		setImage(image);

		health = 90;
		lives = 3;
	}

	void setShooter(Shooter shooter) {
		this.shooter = shooter;
	}

	Shooter getShooter() {
		return shooter;
	}

	@Override
	void update(Level level, double elapsedTime) {
		super.update(level, elapsedTime);
		shooter.setX(getX() + SHOOTER_X_OFFSET);
		shooter.setY(getY() + SHOOTER_Y_OFFSET);
		
		// Change animations
		switch (animationState) {
		case Idle: {
			if (forward) {
				Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_IDLE + ".png"));
				setImage(image);
				SHOOTER_X_OFFSET = 37;
				SHOOTER_Y_OFFSET = 42;

			} else {
				Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_IDLE + "_reverse.png"));
				setImage(image);
				SHOOTER_X_OFFSET = -7;
				SHOOTER_Y_OFFSET = 42;
			}
			break;
		}
		case WalkingRight: {
			Image image = new Image(
					getClass().getClassLoader().getResourceAsStream(IMAGE_WALKING + (int) animTimer + ".png"));
			setImage(image);

			if ((int) animTimer == 1) {
				SHOOTER_X_OFFSET = 38;
				SHOOTER_Y_OFFSET = 37;
			} else {
				SHOOTER_X_OFFSET = 37;
				SHOOTER_Y_OFFSET = 42;
			}
			break;
		}
		case WalkingLeft: {
			Image image = new Image(
					getClass().getClassLoader().getResourceAsStream(IMAGE_WALKING + (int) animTimer + "_reverse.png"));
			setImage(image);
			if ((int) animTimer == 1) {
				SHOOTER_X_OFFSET = -9;
				SHOOTER_Y_OFFSET = 37;
			} else {
				SHOOTER_X_OFFSET = -9;
				SHOOTER_Y_OFFSET = 42;
			}
			break;
		}
		case Jumping: {
			if (forward) {
				Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_JUMPING + ".png"));
				setImage(image);
				SHOOTER_X_OFFSET = 37;
				SHOOTER_Y_OFFSET = 34;
			} else {
				Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_JUMPING + "_reverse.png"));
				setImage(image);
				SHOOTER_X_OFFSET = -10;
				SHOOTER_Y_OFFSET = 34;
			}
			break;
		}
		case Falling: {
			if (forward) {
				Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_FALLING + ".png"));
				setImage(image);
				SHOOTER_X_OFFSET = 40;
				SHOOTER_Y_OFFSET = 35;
			}
			else {
				Image image = new Image(getClass().getClassLoader().getResourceAsStream(IMAGE_FALLING + "_reverse.png"));
				setImage(image);
				SHOOTER_X_OFFSET = -10;
				SHOOTER_Y_OFFSET = 35;
			}
		}
		case Damaged:
		}
	}

	void setInvincible() {
		isInvincible = true;
	}

	void superSpeed() {
		xSpeed = DEFAULT_SPEED * 3;
	}

	void superJump() {
		jumpHeight = DEFAULT_JUMP * 2;
	}

	@Override
	void updateAliveStatus(Level level) {
		if (isInvincible) {
			return;
		}
	}
	
	@Override
	   void updateMovementX(Level level, double elapsedTime) {
        if (isMovingRight) {
            setX(getX() + xSpeed * elapsedTime); //framerate indpp.
        }
        if (isMovingLeft) {
            setX(getX() - xSpeed * elapsedTime);
        }
        for (Platform platform : level.getPlatformList()) {
            Bounds bounds = platform.getLayoutBounds();
            if (intersects(bounds) && platform.isBackground == false) {
                if (isMovingRight) {
                    setX(bounds.getMinX() - imageSize - 0.01);
                }
                if (isMovingLeft) {
                    setX(bounds.getMaxX() + 0.01);
                }
            }
        }
        if (getX() < 0) {
            setX(0);
        }
        if (getY() > 800) {
            setX(Level.levelSpawnx);
            setY(Level.levelSpawny);
            Main.game.sceneRoot.setLayoutX(-1 * getX());
        }
    }

   
	@Override
    void updateMovementY(Level level, double elapsedTime) {
        yVelocity += FALL_ACCEL * elapsedTime;
        setY(getY() + yVelocity * elapsedTime);
        
        for (Platform platform : level.getPlatformList()) {
            Bounds bounds = platform.getLayoutBounds();
            if (intersects(bounds) && platform.isBackground == false && getY() < bounds.getMinY()) {
                setY(bounds.getMinY() - imageHeight - 0.01);
                canJump = true;
                yVelocity = 0;
                yAccel = 0;
                return;
            }
            else if (intersects(bounds) && platform.isBackground == false && getY() > bounds.getMinY()) {
            	System.out.println("Top"); 
            	 setY(bounds.getMinY() + imageHeight + 10);
            	 yVelocity = 0;
                 yAccel = 0;
                 return;
            }
        }
    }

}
