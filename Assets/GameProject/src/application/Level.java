package application; 
import javafx.scene.Group;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class Level extends Group {
    private static final String LEVEL_FILE_PREFIX = "levels/";
    private static final String LEVEL_FILE_POSTFIX = ".txt";
    private static final double PLATFORM_WIDTH_RATIO = .99;
    public static double levelSpawnx;
    public static double levelSpawny;
    private static final int FREE_ID = 0;
    int id;
    private int levelNumber;
    String levelstring; 
    private double levelWidth, levelHeight;
    private List<Platform> platformList;
    private List<Coin> itemList; 
/**
* Load the specified level from file. Initialize lists of characters to
* keep track of. If levelNum is 1, then level1.txt holds level info.
* @param levelNum is level to load.
* @param height is height of the level.
*/
Level(String levelString, int levelNum, double height) {
	ArrayList<TileInfo> level1Key = new ArrayList<TileInfo>(); 
	level1Key.add(new TileInfo("Empty", "/images/coinGold.png", true));
	level1Key.add(new TileInfo("Coin", "/images/coinGold.png", true));
	level1Key.add(new TileInfo("WalkingEnemy", "/images/slimeBlock.png", true));
	level1Key.add(new TileInfo("StaticEnemy", "/images/barnacle.png", true));
	level1Key.add(new TileInfo("Platform", "/images/grassMid.png", false));
	level1Key.add(new TileInfo("Platform", "/images/grassCenter.png", false));
	level1Key.add(new TileInfo("Platform", "/images/signRight.png", true));
	level1Key.add(new TileInfo("FlyingEnemy", "/images/bee.png", true));
	level1Key.add(new TileInfo("Platform", "/images/signExit.png", true));
	level1Key.add(new TileInfo("ShoootingEnemy", "/images/barnacle.png", true));

	ArrayList<TileInfo> holder = new ArrayList<TileInfo>();
	
	levelNumber = levelNum;
    levelstring = levelString;
    String filename = LEVEL_FILE_PREFIX + levelString + levelNum + LEVEL_FILE_POSTFIX;
    
    if (levelString == "level1") {
    	holder = new ArrayList<TileInfo>(level1Key);
    }
    
    System.out.println(filename); 

    InputStream stream = getClass().getClassLoader().getResourceAsStream(filename);
    Scanner input = new Scanner(stream);

    platformList = new ArrayList<Platform>();
    itemList = new ArrayList<Coin>();
    
    int levelBlockHeight = input.nextInt(); 
    input.nextLine(); 
    
	System.out.println(holder.get(1).mImageURL);


    for (int j = 0; j < levelBlockHeight; j ++) {
        String line = input.nextLine(); 
        System.out.println(line); 
        for (int i = 0; i < line.length(); i++) {
            levelHeight = height;
            levelWidth = (double) (line.length()) / levelBlockHeight *
            levelHeight * PLATFORM_WIDTH_RATIO;
            double blockHeight = levelHeight / levelBlockHeight;
            double blockWidth = blockHeight * PLATFORM_WIDTH_RATIO;            
            if (line.charAt(i) != 'p') {
            	id = Character.getNumericValue(line.charAt(i)); 
            }
            else {
            	levelSpawnx = i * PLATFORM_WIDTH_RATIO * blockWidth;
            	levelSpawny = j * PLATFORM_WIDTH_RATIO * blockHeight; 
            	continue; 
            }
            //PLATFORM_WIDTH_RATIO to fix spacing issues
            //Use a map data structure here???
            if (id == FREE_ID) {
                continue;
            }
            else {
            	if (holder.get(id).mType == "Platform") {
            		Platform platform = new Platform(i * PLATFORM_WIDTH_RATIO * blockWidth, j * PLATFORM_WIDTH_RATIO * blockHeight,
            				blockWidth, blockHeight, holder.get(id).mNoCollide, holder.get(id).mImageURL);
            		getChildren().add(platform);
            		platformList.add(platform);  
            	}
            	else if (holder.get(id).mType == "Coin") {
            		Coin coin = new Coin(i * PLATFORM_WIDTH_RATIO * blockWidth, j * PLATFORM_WIDTH_RATIO * blockHeight,
            				Main.game);
            		getChildren().add(coin);
            		
            	}
            }
        }
    }
}

double getHeight() {
    return levelHeight;
}

double getWidth() {
    return levelWidth;
}

int getLevelNumber() {
    return levelNumber;
}

List<Platform> getPlatformList() {
    return platformList;
}

List<Coin> getItemList() {
    return itemList;
}

}