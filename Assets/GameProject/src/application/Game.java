package application; 
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.util.HashSet;
import java.util.Vector;

/**
 * Main game class that handles interactions between all other classes. Updates
 * characters on screen, controls input from player, loads and removes images
 * from the Scene.
 * @author Will Long
 */
class Game {
    private static final int NUM_LEVELS = 3;
    private static final KeyCode CHEAT_KEY = KeyCode.CONTROL;

    private  Scene scene;
    public static Group sceneRoot;
    private int sceneWidth, sceneHeight;

    public Player mPlayer;
    private Image bgImage;
    private Background background;
    private Level currentLevel;
    boolean finishedGame;
    double offset; 
    String levelstring;
    
    private boolean spacePressed;
    
    private Vector<GameCharacter> gameCharacters;
    
    private HashSet<KeyCode> pressedKeys;
    /**
     * Creates a new game of specified size, and loads the starting screen. Sets
     * up game to receive player input from keyboard.
     * @param width is width of game screen.
     * @param height is height of game screen.
     */
    Game(int width, int height, String levelString) {
        sceneWidth = width;
        sceneHeight = height;
        levelstring = levelString;
        sceneRoot = new Group();
        scene = new Scene(sceneRoot, sceneWidth, sceneHeight);

        bgImage = new Image(getClass().getClassLoader().getResourceAsStream("images/clouds.png"));
        
        finishedGame = false;

        spacePressed = false;
        pressedKeys = new HashSet<KeyCode>();
        scene.setOnKeyPressed(e -> pressedKeys.add(e.getCode()));
        scene.setOnKeyReleased(e -> pressedKeys.remove(e.getCode()));
        
        gameCharacters = new Vector<GameCharacter>();
    }

    Scene getScene() {
        return scene;
    }
    
    Group getSceneRoot() {
    	return sceneRoot;
    }
    
    Vector<GameCharacter> getGameCharacters() {
    	return gameCharacters;
    }

    /**
     * Main game loop. Resolves options depending on whether start screen, a
     * level, or the end game mode is currently loaded. If level is currently
     * active, it receives player input, updates all characters, and checks
     * whether the current level should be advanced.
     * @param elapsedTime is time spent updating.
     */
    public void update(double elapsedTime) {
        if (Main.freezeTime == true) { return; } 
    	if (currentLevel == null) {
            if (!finishedGame) {
            	advanceLevels();
            }
            return;
        }
        resolveKeyPresses();
        updateGameCharacters(elapsedTime);
        scrollLevel();
        if (currentLevel != null && mPlayer.getX() + mPlayer.getFitWidth() >= currentLevel.getWidth()) {
            advanceLevels();
        }
        removeDeadCharacters();
    }

    /**
     * Updates position of all characters in game. Updates police to shoot if
     * they are close to mPlayer.
     * @param elapsedTime is time spent updating.
     */
    void updateGameCharacters(double elapsedTime) {
        if (currentLevel == null) {
            return;
        }
        
        for (GameCharacter gc : gameCharacters) {
        	gc.update(currentLevel, elapsedTime);
        }
        
        //mPlayer.update(currentLevel, elapsedTime);
    }

    /**
     * Resolves player input during start screen. Space advances to game, while
     * H triggers help message.
     */

    void resolveKeyPresses() {
        if (pressedKeys.contains(CHEAT_KEY)) {
            resolveCheatCode();
            return;
        }

        if (pressedKeys.contains(KeyCode.ESCAPE)) {
        		try {
        		Pane newLoadedPane =  FXMLLoader.load(getClass().getResource("/application/MainMenu.fxml")); // Good meme
        		
        		newLoadedPane.setLayoutX(mPlayer.getX() - 305); // Even better meme
        		
        		Game.sceneRoot.getChildren().add(newLoadedPane);
        		} catch (IOException ioe) {
        			System.out.println(ioe.getMessage()) ;
        		}
        		
        		Main.freezeTime = true; 
        }
        
        if (pressedKeys.contains(KeyCode.D) && !pressedKeys.contains(KeyCode.A)) {
            mPlayer.moveRight();
        } else if (pressedKeys.contains(KeyCode.A) && !pressedKeys.contains(KeyCode.D)) {
            mPlayer.moveLeft();
        } else {
            mPlayer.stall();
        }

        if (pressedKeys.contains(KeyCode.W)) {
            mPlayer.jump();
        }
        
		if (pressedKeys.contains(KeyCode.SPACE)) {
			if (!spacePressed) {
				Projectile projectile = new Projectile(mPlayer.getShooter().getX(), mPlayer.getShooter().getY() + 5, this, mPlayer);
				sceneRoot.getChildren().add(projectile);
			}
			spacePressed = true;
        }
		else {
			spacePressed = false;
		}
    }

    /**
     * Resolves cheat codes. Numerous cheats are available when holding control
     * key, including upgrades to mPlayer, skipping levels, clearing enemies.
     */
    void resolveCheatCode() {
        if (pressedKeys.contains(KeyCode.D)) {
            mPlayer.setX(mPlayer.getX() + 100);
            mPlayer.setY(0);
        }
        if (pressedKeys.contains(KeyCode.A)) {
            mPlayer.setX(mPlayer.getX() - 100);
            mPlayer.setY(0);
        }

        if (pressedKeys.contains(KeyCode.I)) {
            mPlayer.setInvincible();
        }
        if (pressedKeys.contains(KeyCode.F)) {
            mPlayer.superSpeed();
        }
        if (pressedKeys.contains(KeyCode.J)) {
            mPlayer.superJump();
        }
        if (pressedKeys.contains(KeyCode.C)) {
            Player newmPlayer = new Player(mPlayer.getX(), mPlayer.getY(), this);
            newmPlayer.toBack();
            sceneRoot.getChildren().remove(mPlayer);
            mPlayer = newmPlayer;
            sceneRoot.getChildren().add(mPlayer);
        }
        for (KeyCode kc : pressedKeys) {
            if (kc.isDigitKey()) {
                int level = Integer.parseInt(kc.getName());
                skipToLevel(level);
                break;
            }
        }
    }

    /**
     * Checks if any game characters have died, and removes them if they have.
     */
    void removeDeadCharacters() {
        if (!mPlayer.isAlive()) {
            skipToLevel(0);
            return;
        }
 
        for (GameCharacter gc : gameCharacters) {
        	if (!gc.isAlive()) {
        		gameCharacters.remove(gc);
        		sceneRoot.getChildren().remove(gc);
        	}
        }

        if (currentLevel == null) {
            return;
        }
    }

    /**
     * Scrolls level according to mPlayer's position.
     */
    void scrollLevel() {
        offset = (sceneWidth * 1.0 / 3) - 5;
        if (mPlayer.getX() < offset) {
            return;
        }
        sceneRoot.setLayoutX(-1 * mPlayer.getX() + offset);
        double minLayout = -1 * currentLevel.getWidth() + sceneWidth;
        if (sceneRoot.getLayoutX() < minLayout + 25) {
            sceneRoot.setLayoutX(minLayout + 25);
        }
    }

    /**
     * Advances to the next level. If on start screen, goes to first main level.
     * If finished last main level, advances to end game. Otherwise, advances to
     * next main level whenever mPlayer reaches end of current level.
     */
    void advanceLevels() {
        sceneRoot.getChildren().clear();
        sceneRoot.setLayoutX(0);

        int levelNum = 0;
        if (currentLevel != null) {
            levelNum = currentLevel.getLevelNumber();
        }
        if (levelNum == NUM_LEVELS) {
            currentLevel = null;
            finishedGame = true;
            return;
        }

        currentLevel = new Level(levelstring, levelNum + 1, sceneHeight);

        mPlayer = new Player(Level.levelSpawnx, Level.levelSpawny, this);
        background = new Background(bgImage, currentLevel.getWidth(), currentLevel.getHeight());

        sceneRoot.getChildren().add(background);
        sceneRoot.getChildren().add(currentLevel);
        sceneRoot.getChildren().add(mPlayer);
        
        Shooter shooter = new Shooter(45, -50, this);
        mPlayer.setShooter(shooter);
        sceneRoot.getChildren().add(shooter);
    }

    /**
     * Skips to the specified level, or to a start/end screen if a lower/higher
     * level number is provided than is possible in game.
     * @param levelNum is level to move to.
     */
    void skipToLevel(int levelNum) {
        sceneRoot.getChildren().clear();
        sceneRoot.setLayoutX(0);
        currentLevel = null;

        if (levelNum <= 0) {
            finishedGame = false;
        } else if (levelNum > NUM_LEVELS) {
            finishedGame = true;
        } else {
            mPlayer = new Player(Level.levelSpawnx, Level.levelSpawny, this);
            currentLevel = new Level(levelstring, levelNum, sceneHeight);
            background = new Background(bgImage, currentLevel.getWidth(), currentLevel.getHeight());

            sceneRoot.getChildren().add(background);
            sceneRoot.getChildren().add(currentLevel);
            sceneRoot.getChildren().add(mPlayer);
            
            Shooter shooter = new Shooter(45, -50, this);
            mPlayer.setShooter(shooter);
            sceneRoot.getChildren().add(shooter);
        }
    }
}

